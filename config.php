<?php 

return [
    'debug' => false,
    'database' => [
        'hostname' => 'localhost',
        'username' => 'root',
        'password' => 'password',
        'database' => 'pokedex',
    ]
];