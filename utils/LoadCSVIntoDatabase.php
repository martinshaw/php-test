<?php

require __DIR__ . '/../vendor/autoload.php';

// Run this script using the 'PHP' command line interface, using the path to a CSV file as the CLI argument

// Example CSV Format: (with double comma as delimiter)
//		image_path,,name,,species_type,,height,,weight,,abilities
//      /img/test.jpg,,Pikachu,,Mouse Pokemon,,0.4,,6,,Static, Lightningrod

// Example script usage:
// "\Program Files (x86)\PHP\v5.3\php.exe" LoadCSVIntoDatabase.php csv/first_dataset.csv


if(!isset($argv[1])){
	echo "You need to provide a path to a valid CSV file! Exiting";
	exit(1);
}

if(!file_exists($argv[1])){
	echo "Cannot find a valid CSV file at that path! Exiting";
	exit(1);
}


// =====================================================================
// Preparing database ORM object

echo "Preparing DB Connection...\n\n";
$db = new \App\Database;


// =====================================================================
// Attempt to read CSV file

echo "Attempting to read CSV file...\n\n";

$handle = fopen($argv[1], "r");
$line_count = 0;

if ($handle) {
    while (($line = fgets($handle)) !== false) {
    	if($line_count != 0){

    		$data = explode(",", $line);

    		echo "Line ".$line_count.":  ";
    		var_dump($data);
    		echo "\n\n";

    		echo "Inserting data into table...\n\n";

			$db->insertPokemon($data[0], $data[1], $data[2], $data[3], $data[4], $data[5]);

    	} 
    	$line_count++;
    }

    fclose($handle);
} else {
	echo "Could not read file! Exiting";
	exit(1);
} 