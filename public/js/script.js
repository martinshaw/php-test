var hideLoading = setTimeout(function(){
	$(".pokedex__loading").addClass("close");
}, 1000);


var loadInitialPokemon = function() {
	$.get('/api/pokemon', function(data) {
		$(".pokedex__container__search__list").html("");

		for (var i = 0; i < data.data.length; i++) {
			let template = `<div class="pokedex__container__search__list__item" data-id="${data.data[i].id}">
							<div class="pokedex__container__search__list__item__thumbnail" style="background-image: url(${data.data[i].image_path});">&nbsp;</div>
							<div class="pokedex__container__search__list__item__name">${data.data[i].name}</div>
						</div>`;

			let element = $(template);
			element.click(function(){
				changeBrowseView(element.attr("data-id"));
			})

			$(".pokedex__container__search__list").append(element);
		}

	});
}


var performSearch = function() {
	$.get('/api/pokemon?query='+$(".pokedex__container__search__input__field").val(), function(data) {
		$(".pokedex__container__search__list").html("");

		for (var i = 0; i < data.data.length; i++) {
			let template = `<div class="pokedex__container__search__list__item" data-id="${data.data[i].id}">
							<div class="pokedex__container__search__list__item__thumbnail" style="background-image: url(${data.data[i].image_path});">&nbsp;</div>
							<div class="pokedex__container__search__list__item__name">${data.data[i].name}</div>
						</div>`;

			let element = $(template);
			element.click(function(){
				changeBrowseView(element.attr("data-id"));
			})

			$(".pokedex__container__search__list").append(element);
		}

	});
}


var changeBrowseView = function (id){
	$.get('/api/pokemon?id='+id, function(data) {
		$(".pokedex__container__welcome").css("display", "none");
		$(".pokedex__container__browse").addClass("active");

		$(".pokedex__container__browse__title").text(data.data[0].name);
		$(".pokedex__container__browse__image").css("background-image", "url("+data.data[0].image_path+")");
		$(".item_species").html("<b>Species Type:</b><br/><br/>"+data.data[0].species_type);
		$(".item_height").html("<b>Height:</b><br/><br/>"+data.data[0].height + " metres");
		$(".item_weight").html("<b>Weight:</b><br/><br/>"+data.data[0].weight + " kg");
	});
}

$(document).ready(function(){

	// Load all Pokemon into search section
	loadInitialPokemon();

	// Setup event listeners for search bar
	$(".pokedex__container__search__input__field").keyup(performSearch);

	// Setup event listeners for Pokemon links
	setupPokemonItemClickListener();

});