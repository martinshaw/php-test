<?php
namespace App;

class Application extends Singleton
{
	protected $rootPath = '';
	protected $appPath = '';

	protected $config;
	protected $request;
	protected $router;

	public function __construct() {
		$this->appPath = dirname(__FILE__);
		$this->rootPath = realpath($this->appPath . '/..');

		$this->config = Config::getInstance();
		$this->request = Request::getInstance();
		$this->router = Router::getInstance();
	}

	public function run() {
		Response::handleResponse($this->router->dispatch());
	}

	public function getRootPath() {
		return $this->rootPath;
	}

	public function getAppPath() {
		return $this->appPath;
	}
}