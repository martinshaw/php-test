<?php
namespace App;

class Request extends Singleton
{
	protected $rawPath;
	protected $path;

	public function __construct() {
		$this->rawPath = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$this->path = parse_url($this->rawPath);
	}

	public function getPath() {
		return $this->path;
	}
}