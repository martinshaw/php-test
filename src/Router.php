<?php
namespace App;

class Router extends Singleton
{
	use FetchesArray;

	protected $routes;

	public function __construct() {
		$this->routes = $this->fetchArray('../routes.php');
	}

	public function dispatch() {
		$route = $this->findRouteByRequest(Request::getInstance());
		try {
			return $this->handleRoute($route);
		} catch (\Exception $exception) {
			return $this->handleRoute($this->getExceptionRoute(), [$exception]);
		}
	}

	protected function findRouteByRequest($request) {
		foreach ($this->getRouteNames() as $routeName) {
			$matches = [];
			preg_match_all('/^'.str_replace('/', '\\/', $routeName).'$/i', $request->getPath()['path'], $matches, PREG_SET_ORDER, 0);
			if (isset($matches[0])) {
				return $this->routes[$matches[0][0]];
			}
		}
		return $this->getNotFoundRoute();
	}

	protected function handleRoute($route, $args = []) {
		return call_user_func_array($route, $args);
	}

	protected function getRouteNames() {
		return array_keys($this->routes);
	}

	protected function getNotFoundRoute() {
		return function () {
			return \App\Response::fromHtmlFile('/templates/404.html', 404);
		};
	}

	protected function getExceptionRoute() {
		return function ($exception) {
			return Config::getInstance()->get('debug') ?
				'Debug Mode - Exception Thrown: ' . $exception->getMessage() :
				\App\Response::fromHtmlFile('/templates/500.html', 500);
		};
	}
}