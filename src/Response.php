<?php
namespace App;

class Response
{
	protected $content = '';
	protected $statusCode = 200;
	protected $headers = [];

	public function __construct($content, $statusCode = 200, $headers = []) {
		$this->content = $content;
		$this->statusCode = $statusCode;
		$this->headers = $headers;
	}

	public static function fromHtmlFile($path, $statusCode = 200) {
		$fullPath = Application::getInstance()->getAppPath() . $path;
		if (is_file($fullPath)) {
			return new static(file_get_contents($fullPath), $statusCode, []);
		}
		throw new \Exception('Cannot find HTML file at the provided path');
	}

	public static function fromArray($array) {
		return new static(
			json_encode(["data" => $array]),
			200,
			['Content-Type' => 'application/json']
		);
	}

	public static function handleResponse($response) {
		if ($response instanceof Response === false) {
			http_response_code(200);
			ob_start();
			echo $response;
			ob_end_flush();
		}

		http_response_code($response->statusCode);
		foreach ($response->headers as $title => $value) {		
			header("$title: $value");
		}
		ob_start();
		echo $response->content;
		ob_end_flush();
	}
}