<?php
namespace App;

class Config extends Singleton
{
	use FetchesArray;

	protected $settings;

	public function __construct() {
		$this->settings = $this->fetchArray('../config.php');
	}

	public function get($name, $default = null) {
		return isset($this->settings[$name]) ? $this->settings[$name] : $default;
	}
}