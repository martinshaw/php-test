<?php
namespace App;

class Pokemon
{
    public $image_path = '/img/pokemon/0.png';
    public $name = 'Default Pokemon';
    public $species_type = 'Generic';
    public $height = 0;
    public $weight = 0;
	public $abilities = 'none';
	
	public function __construct($properties) {
		foreach ($properties as $property => $value) {
			$this->{$property} = $value;
		}
	}

    public function toJSON(){
		return array(
	        "id" => $this->id,
	        "image_path" => $this->image_path,
	        "name" => $this->name,
	        "species_type" => $this->species_type,
	        "height" => $this->height,
	        "weight" => $this->weight,
	        "abilities" => $this->abilities
	    );
	}
}