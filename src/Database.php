<?php
namespace App;

class Database extends Singleton {
	public $is_error = false;
	public $error_msg = "";

	public $conn = "";

	// Create connection
	function makeConnection(){
		$config = Config::getInstance()->get('database');
		$this->conn = new \mysqli($config['hostname'], $config['username'], $config['password'], $config['database']);

		// Check connection
		if ($this->conn->connect_error) {
			$this->is_error = true;
			$this->error_msg = $this->conn->connect_error . "";
			throw new \Exception('Cannot connect to database with the provided credentials');
		} 
		return true;		
	}

	// Get all pokemon
	function getAllPokemon(){
		$this->makeConnection();

		$result = $this->conn->query("SELECT * FROM pokemon");

		$items = [];
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$pokemon = new Pokemon([
					'id' => $row['id'],
					'image_path' => $row['image_path'],
					'name' => $row['name'],
					'species_type' => $row['species_type'],
					'height' => $row['height'],
					'weight' => $row['weight'],
					'abilities' => $row['abilities'],
				]);
				array_push($items, $pokemon->toJSON());
			}
		}
		$this->conn->close();
		return $items;
	}


	// Get pokemon by ID
	function getPokemonByID($id){
		$this->makeConnection();

		$sql = 'SELECT * FROM pokemon where id=\''.mysqli_real_escape_string($this->conn, $id).'\' limit 1';
		$result = $this->conn->query($sql);

		$items = [];
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$pokemon = new Pokemon([
					'id' => $row['id'],
					'image_path' => $row['image_path'],
					'name' => $row['name'],
					'species_type' => $row['species_type'],
					'height' => $row['height'],
					'weight' => $row['weight'],
					'abilities' => $row['abilities'],
				]);
				array_push($items, $pokemon->toJSON());
			}
		}
		$this->conn->close();
		return $items;
	}

	// Get pokemon by ID
	function insertPokemon($image_path, $name, $species_type, $height, $weight, $abilities){
		$this->makeConnection();

		$sql = 'INSERT INTO pokemon (image_path, name, species_type, height, weight, abilities) VALUES (\''.
			mysqli_real_escape_string($this->conn, $image_path).'\',\''.
			mysqli_real_escape_string($this->conn, $name).'\',\''.
			mysqli_real_escape_string($this->conn, $species_type).'\',\''.
			mysqli_real_escape_string($this->conn, $height).'\',\''.
			mysqli_real_escape_string($this->conn, $weight).'\',\''.
			mysqli_real_escape_string($this->conn, $abilities).'\')';

		if ($this->conn->query($sql) === TRUE) {
			return true;
		} else {
			throw new \Exception('An error occurred when attempting to insert new Pokemon: ' . $this->conn->error);
		}

		$this->conn->close();
	}

	// Get pokemon according to a search term
	function getPokemonBySearchTerm($query){
		$this->makeConnection();

		$sql = 'SELECT * FROM pokemon WHERE LOWER(name) LIKE \'%'.mysqli_real_escape_string($this->conn, $query).'%\'';
		$result = $this->conn->query($sql);

		$items = [];
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$pokemon = new Pokemon([
					'id' => $row['id'],
					'image_path' => $row['image_path'],
					'name' => $row['name'],
					'species_type' => $row['species_type'],
					'height' => $row['height'],
					'weight' => $row['weight'],
					'abilities' => $row['abilities'],
				]);
				array_push($items, $pokemon->toJSON());
			}
		}
		$this->conn->close();
		return $items;
	}

}

