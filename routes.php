<?php 

return [
    '/' => function () {
        return \App\Response::fromHtmlFile('/templates/index.html');
    },
    '/api/pokemon' => function () {
        if (isset($_GET['id'])) {
            $items = \App\Database::getInstance()->getPokemonByID($_GET['id']);
        } else if (isset($_GET['query'])) {
            $items = \App\Database::getInstance()->getPokemonBySearchTerm($_GET['query']);
        } else {
            $items = \App\Database::getInstance()->getAllPokemon();
        }
        return \App\Response::fromArray($items);
    }
];